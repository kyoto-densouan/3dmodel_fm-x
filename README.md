# README #

1/3スケールの富士通 FM-X風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- 富士通

## 発売時期
- FM-X 1983年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/FM-X)
- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1089380.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-x/raw/0b6f78e391806e6e8629a5cbc92518430766834c/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-x/raw/0b6f78e391806e6e8629a5cbc92518430766834c/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-x/raw/0b6f78e391806e6e8629a5cbc92518430766834c/ExampleImage.png)
